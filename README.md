Start project
```
### install dependencies
npm install

### DB migration
adonis migration:run

### start server
adonis serve --dev
```
