'use strict'
const { validate } = use('Validator');
const Note = use('App/Models/Note');
const User = use('App/Models/User');


class NoteController {

  async createNote({request, response, auth}) {

    const { noteHeader, noteBody } = request._body
    if ((!noteBody && !noteHeader)) {
      return response.status(400).send('Fill title of text of note')
    }
    // const validation = await validate({ note_header, note_body }, rules)

    // if (!validation.fails()) {
    if (true) {

      try {
        const user = await auth.getUser();
        const note = new Note()
        note.note_header = noteHeader
        note.note_body = noteBody
        note.user_id = user.id

        await user.notes().save(note)
        const notes = await user.notes().where('user_id', user.id).fetch()
        // return response.status(200).send('Note was created', notes)
        return response.status(200).send(notes)

      } catch (e) {
        response.status(401).send({ error: 'something went wrong' });
      }
    } else {
      response.status(401).send(validation.messages());
    }
  }

  async getNotes({response, auth}) {
    try {
      const user = await auth.getUser();
      const allNotes = await user.notes().where('user_id', user.id).fetch()
      return response.status(200).send(allNotes)
    } catch (e) {
      response.status(401).send({error: 'something went wrong'})
    }
  }

  async updateNote({request, response, auth}) {
    try {
      const user = await auth.getUser();
      const newNote = request.body;
      await user.notes().where({user_id: user.id, id: newNote.id}).update(newNote)
      const allNotes = await user.notes().where('user_id', user.id).fetch()
      return response.status(200).send(allNotes)
    } catch (e) {
      response.status(401).send({error: 'something went wrong'})
    }
  }

  async deleteNote({params, response, auth}) {
    try {
      const user  = await auth.getUser();
      const deleteNoteId = params.id;
      await user.notes().where({user_id: user.id, id: deleteNoteId}).delete()
      const allNotes = await user.notes().where('user_id', user.id).fetch()
      return response.status(200).send(allNotes)
    } catch (e) {
      response.status(401).send({error: 'something went wrong'})
    }
  }
}

module.exports = NoteController


//@TODO vratit spat notes po crud operacii - ako?
//@TODO vytvaranie usera refactor - ako?
//@TODO authentication middleware a ako nastavit privatne routy?
//@TODO nejde prihlasit pomocou mena, len pomocou hesla

