'use strict'

const User = use('App/Models/User')

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
const Database = use('Database')

Route.get('/health', () => 'Healthy');

Route.group(() => {
  /*
  |--------------------------------------------------------------------------h
  | API - Authentication
  |--------------------------------------------------------------------------
  */
  Route.post('/login', 'AuthController.signIn');
  Route.post('/register', 'AuthController.register');
  Route.post('/auth/token/refresh', 'AuthController.refreshToken');
  Route.post('/logout', 'AuthController.logout');

  /*
  |--------------------------------------------------------------------------
  | API - User
  |--------------------------------------------------------------------------
  */
  Route.get('/user', 'UserController.getUser');

  Route.post('/note', 'NoteController.createNote');
  Route.get('/notes', 'NoteController.getNotes');
  Route.patch('/note', 'NoteController.updateNote');
  Route.delete('/note/:id', 'NoteController.deleteNote');
})

